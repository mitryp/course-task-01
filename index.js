// TODO
import express from 'express';
import {baseRouter} from './router/router.js';
import {port} from './constants.js';

const app = express();

app.use(
    express.text({type: 'text/plain'}),
    express.json({type: 'application/json'}),
    baseRouter(),
);

app.listen(port, () => console.log(`listening on http://localhost:${port}`));
