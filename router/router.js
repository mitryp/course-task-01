import {Router} from 'express';
import {squareController} from '../controllers/squareController.js';
import {reverseController} from '../controllers/reverseController.js';
import {dateController} from '../controllers/dateController.js';

export function baseRouter() {
    const router = new Router();

    router.post('/square', squareController); // todo
    router.post('/reverse', reverseController); // todo
    router.get('/date/:year/:month/:day', dateController); // todo [req.params...]

    return router;
}