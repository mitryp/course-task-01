/**
 * @param req {Request}
 * @param res {Response}
 */
export function reverseController(req, res) {
    const resText = req.body ?? '';

    const reversed = resText.split('').reverse().join('');

    res.send(reversed);
}
