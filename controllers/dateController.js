import {dateDifference} from "../util/dateDifference.js";
import {isLeapYear} from "../util/leapYears.js";

/**
 * @param req {Request}
 * @param res {Response}
 */
export function dateController(req, res) {
    const year = req.params.year,
        month = req.params.month,
        day = req.params.day;

    // if (!validateDate(year, month, day))
    //     return badRequest(res, 'Date format is incorrect');

    const date = new Date(Date.UTC(+year, +month - 1, +day));
    const today = new Date(Date.now());

    res.send({
        weekDay: weekdays[date.getUTCDay()],
        isLeapYear: isLeapYear(date.getUTCFullYear()),
        difference: dateDifference(date, new Date(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate())),
    });
}

const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];