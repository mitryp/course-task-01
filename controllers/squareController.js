import {badRequest} from "../util/responses.js";

export function squareController(req, res) {
    const bodyNumber = req.body;

    if (!/\d+/g.test(bodyNumber))
        return badRequest(res);

    const number = parseFloat(bodyNumber);

    res.send({
        number: number,
        square: number * number,
    });
}
