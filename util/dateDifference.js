export function dateDifference(date1, date2) {
    const msDiff = Math.abs(date1.getTime() - date2.getTime());

    return Math.round(msDiff / (1000 * 60 * 60 * 24));
}
