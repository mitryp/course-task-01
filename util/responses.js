
export function badRequest(res, msg = null) {
    res.statusCode = 400;
    if (msg === null)
        res.send();
    else
        res.send({message: msg});
}


