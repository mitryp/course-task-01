function validateDay(day) {
    return day && /((3[01])|([21]\d)|(0[1-9]))/g.test(day);
}

function validateMonth(month) {
    return month && /(0[1-9])|(1[0-2])/g.test(month);
}

function validateYear(year) {
    return year && /\d\d\d\d/g.test(year);
}

export function validateDate(year, month, day) {
    return validateYear(year) && validateMonth(month) && validateDay(day);
}
